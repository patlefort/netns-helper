#!/usr/bin/env bash

set -euo pipefail
#set -x
shopt -s inherit_errexit

readonly avail_features=(dhcp dhcp6 macvlan nat bridged dnsmasq dhcpcd)
readonly config_dir='/etc/netns-helper'

declare -A opts

pargs=()
while [[ $# -gt 0 ]]; do
	if [[ $1 == '-'* ]]; then
		case "$1" in
			--)
				shift 1
				break
			;;
			--parent_if)
				opts[parent_if]="$2"
				shift 2
			;;

			--mac)
				opts[mac]="$2"
				shift 2
			;;

			--macvlan_mode)
				opts[macvlan_mode]="$2"
				shift 2
			;;

			--privacy_ext)
				opts[privacy_ext]="$2"
				shift 2
			;;

			--ipaddr4)
				opts[idaddr4]="$2"
				shift 2
			;;

			--ipaddr6)
				opts[idaddr6]="$2"
				shift 2
			;;

			--ipaddr4_peer)
				opts[idaddr4_peer]="$2"
				shift 2
			;;

			--ipaddr6_peer)
				opts[idaddr6_peer]="$2"
				shift 2
			;;

			--default_gateway4)
				opts[default_gateway4]="$2"
				shift 2
			;;

			--default_gateway6)
				opts[default_gateway6]="$2"
				shift 2
			;;

			--gateway_metric)
				opts[gateway_metric]="$2"
				shift 2
			;;

			--bridge_interface)
				opts[bridge_interface]="$2"
				shift 2
			;;

			--overwrite)
				opts[overwrite]='yes'
				shift 1
			;;

			--now)
				opts[now]='yes'
				shift 1
			;;

			--help)
				opts[help]='yes'
				shift 1
			;;

			*)
				>&2 echo 'Invalid option.'
				exit 1
			;;
		esac
	else
		pargs+=($1)
		shift 1
	fi
done

_opt_is()
{
	[[ -v opts["$1"] && "${opts["$1"]}" == "$2" ]]
}

_has_opt()
{
	[[ -v opts["$1"] && -n "${opts["$1"]}" ]]
}

show_help()
{
	cat <<-EOF
		Switches:
		  --now                          Start, restart or stop services now.
		  --overwrite                    Overwrite config with new config if applicable to feature.
		  --parent_if <interface>        Parent interface for macvlan.
		  --mac <mac address>            MAC address of macvlan interface.
		  --macvlan_mode <mode>          macvlan mode (bridge, vepa, private).
		  --privacy_ext <value>          Privacy extensions (0, 1 or 2, see sysctl use_tempaddr).
		  --ipaddr4 <address>            IPv4 address of interface.
		  --ipaddr4_peer <address>       IPv4 address of peer interface.
		  --ipaddr6 <address>            IPv6 address of interface.
		  --ipaddr6_peer <address>       IPv6 address of peer interface.
		  --default_gateway4 <address>   IPv4 default gateway.
		  --default_gateway6 <address>   IPv6 default gateway.
		  --gateway_metric <value>       Gateway metric.
		  --bridge_interface <interface> Bridge interface to add an interface to.

		Commands:
		  enable <namespace> [<list of features>]
		  disable <namespace> [<list of features>]
		  purge <namespace>
		  add-service <service> <namespace>
		  remove-service <service>
		  status [<namespace>] [<list of features>]
		  start|restart|stop [<namespace>] [<list of features>]
		  addr [<namespace>]
		  route [<namespace>]
		  ruleset [<namespace>]
		  exec [<namespace>] -- <command>
		  help

		Features:
		  dhcp                           Start a dhcp client inside namespace.
		  dhcp6                          Start a dhcpv6 client inside namespace.
		  macvlan                        Create a macvlan interface inside namespace.
		  nat                            Connect to network with a veth pair and NAT.
		  bridged                        Create a veth pair inside namespace and add it to a bridge.
		  dnsmasq                        Use dnsmasq for DNS resolution inside namespace.
		  dhcpcd                         Use dhcpcd for DHCP inside namespace.
	EOF
}

if _opt_is help yes; then
	show_help
	exit
fi

flags=()
if _opt_is now yes; then flags+=(--now); fi

tput_checked()
{
	tput "$@" 2>/dev/null || true
}
readonly ocol_label="$(tput_checked setaf 3)"
readonly ocol_sublabel="$(tput_checked setaf 5)"
readonly ocol_reset="$(tput_checked sgr0)"

echo_title()
{
	echo
	echo -e " - ${ocol_label}$1${ocol_reset} - "
	echo
}

echo_subtitle()
{
	echo -e "  [${ocol_sublabel}$1${ocol_reset}]"
}

read_service()
{
	local service="$1"

	[[ -n "$service" ]] || \
		{
			>&2 echo 'No service given.'
			exit 1
		}

	if [[ $service != *.service ]]; then
		service="${service}.service"
	fi

	[[ -n $(systemctl list-unit-files --full --plain --all --no-legend --type service "$service") ]] || \
		{
			>&2 echo "Service \`$service\` not found."
			return 1
		}

	echo "$service"
}

feature_is_valid()
{
	for f in ${avail_features[@]}; do [[ "$f" == "$1" ]] && return 0; done
	return 1
}

add_features()
{
	local -r NS=$1
	shift 1

	for feature in "$@"; do
		feature_is_valid "$feature" ||
			{
				>&2 echo "Invalid feature \`$feature\`."
				return 1
			}
	done

	mkdir -p "$config_dir/ns/$NS"
	mkdir -p "/etc/netns/$NS"

	[[ -e "/etc/netns/$NS/resolv.conf" ]] || touch "/etc/netns/$NS/resolv.conf"

	for feature in "$@"; do
		case "$feature" in
			macvlan)
				if _opt_is overwrite yes || [[ ! -f "$config_dir/ns/$NS/macvlan.conf" ]]; then
					_has_opt parent_if || \
						{
							>&2 echo "The parent interface of the macvlan interface must be specified with option \`--parent_if <interface>\`."
							exit 1
						}

					umask 022
					cat > "$config_dir/ns/$NS/macvlan.conf" <<- EOF
						parent_if=${opts[parent_if]}
						mac=${opts[mac]:-}
						macvlan_mode=${opts[macvlan_mode]:-}
						privacy_ext=${opts[privacy_ext]:-}
						ipaddr4=${opts[ipaddr4]:-}
						default_gateway4=${opts[default_gateway4]:-}
						ipaddr6=${opts[ipaddr6]:-}
						default_gateway6=${opts[default_gateway6]:-}
						gateway_metric=${opts[gateway_metric]:-}
					EOF

					[[ -f "$config_dir/ns/$NS/macvlan.conf" ]] && \
						echo "Configuration file \`$config_dir/ns/$NS/macvlan.conf\` overwritten."
				else
					[[ -f "$config_dir/ns/$NS/macvlan.conf" ]] && \
						echo "Configuration file \`$config_dir/ns/$NS/macvlan.conf\` already exists and will not be overwritten."
				fi
			;;

			nat)
				if _opt_is overwrite yes || [[ ! -f "$config_dir/ns/$NS/nat.conf" ]]; then
					[[ -f "$config_dir/ns/$NS/nat.conf" ]] && \
						echo "Configuration file \`$config_dir/ns/$NS/nat.conf\` overwritten."

					umask 022
					cat > "$config_dir/ns/$NS/nat.conf" <<- EOF
						parent_if=${opts[parent_if]:-}
						ipaddr4=${opts[ipaddr4]:-}
						ipaddr4_peer=${opts[ipaddr4_peer]:-}
						default_gateway4=${opts[default_gateway4]:-}
						ipaddr6=${opts[ipaddr6]:-}
						ipaddr6_peer=${opts[ipaddr6_peer]:-}
						default_gateway6=${opts[default_gateway6]:-}
						gateway_metric=${opts[gateway_metric]:-}
					EOF
				fi
			;;

			bridged)
				if _opt_is overwrite yes || [[ ! -f "$config_dir/ns/$NS/bridged.conf" ]]; then
					_has_opt bridge_interface || \
						{
							>&2 echo "The bridge interface must be specified with option \`--bridge_interface <interface>\`."
							exit 1
						}

					[[ -f "$config_dir/ns/$NS/bridged.conf" ]] && \
						echo "Configuration file \`$config_dir/ns/$NS/bridged.conf\` overwritten."

					umask 022
					cat > "$config_dir/ns/$NS/bridged.conf" <<- EOF
						bridge_interface=${opts[bridge_interface]}
						mac=${opts[mac]:-}
						ipaddr4_peer=${opts[ipaddr4_peer]:-}
						default_gateway4=${opts[default_gateway4]:-}
						ipaddr6_peer=${opts[ipaddr6_peer]:-}
						default_gateway6=${opts[default_gateway6]:-}
						gateway_metric=${opts[gateway_metric]:-}
					EOF
				fi
			;;

			dnsmasq)
				if [[ ! -e "/etc/netns/$NS/nsswitch.conf" ]]; then
					cp '/etc/nsswitch.conf' "/etc/netns/$NS/nsswitch.conf"
					sed -i 's/resolve \[!UNAVAIL=return\]//;s/resolve//' "/etc/netns/$NS/nsswitch.conf"
				fi
			;;
		esac

		systemctl enable netns-helper-$feature@$NS.service "${flags[@]}"
	done
}

get_namespaces()
{
	local -r ns=${1:-}

	if [[ -z $ns ]]; then
		namespaces=$(systemctl list-units --type target "netns-helper@*.target" --all --no-legend --plain --full \
			| sed -r 's/^(\s*)netns-helper@(\S+)\.target(.*)/\2/')
	else
		namespaces="$ns"
	fi
}

case ${pargs[0]:-} in
	help)
		show_help
	;;

	add-service)
		readonly service="$(read_service "${pargs[1]}")"
		readonly ns=${pargs[2]:-}

		[[ -n "$ns" ]] || \
			{
				>&2 echo 'No namespace given.'
				exit 1
			}

		umask 022

		mkdir -p "/etc/systemd/system/$service.d"
		cat > "/etc/systemd/system/$service.d/netns-helper.conf" <<- EOF
			[Unit]
			After=netns-helper@${ns}.target
			Requisite=netns-helper@${ns}.target
			PartOf=netns-helper@${ns}.target
			JoinsNamespaceOf=netns-helper@${ns}.service
			ConditionPathExists=/var/run/netns/${ns}

			[Service]
			PrivateNetwork=true

			BindPaths=-/etc/netns/${ns}/resolv.conf:/etc/resolv.conf
			BindPaths=-/etc/netns/${ns}/nsswitch.conf:/etc/nsswitch.conf
		EOF

		systemctl daemon-reload

		if _opt_is now yes; then
			systemctl restart $service
		fi

		echo "Service \`$service\` added to network namespace \`$ns\`."
	;;

	remove-service)
		readonly service="$(read_service "${pargs[1]}")"

		rm -f "/etc/systemd/system/$service.d/netns-helper.conf"

		systemctl daemon-reload

		if _opt_is now yes; then
			systemctl restart $service
		fi

		echo "Service \`$service\` removed from network namespace."
	;;

	enable)
		readonly ns=${pargs[1]:-}

		[[ -n "$ns" ]] || \
			{
				>&2 echo 'No namespace given.'
				exit 1
			}

		systemctl enable netns-helper@$ns.service

		add_features $ns "${pargs[@]:2}"

		systemctl enable netns-helper@$ns.target "${flags[@]}"
	;;

	disable)
		readonly ns=${pargs[1]:-}

		[[ -n "$ns" ]] || \
			{
				>&2 echo 'No namespace given.'
				exit 1
			}

		features=(${pargs[@]:2})

		if [[ ${#features[@]} == 0 ]]; then
			echo "Disabling namespace \`$ns\`..."
			systemctl disable netns-helper@$ns.target "${flags[@]}"
		else
			for feature in ${features[@]}; do
				echo "Disabling feature \`$feature\` in \`$ns\`..."
				systemctl disable netns-helper-$feature@$ns.service "${flags[@]}"
			done
		fi
	;;

	purge)
		readonly ns=${pargs[1]:-}

		[[ -n "$ns" ]] || \
			{
				>&2 echo 'No namespace given.'
				exit 1
			}

		echo "Purging namespace \`$ns\`..."

		systemctl disable netns-helper@$ns.target "${flags[@]}"
		systemctl disable netns-helper@$ns.service "${flags[@]}"

		for feature in ${avail_features[@]}; do
			systemctl disable netns-helper-$feature@$ns.service "${flags[@]}"
		done

		rm -rf "/etc/netns/$ns"
		rm -rf "/etc/netns-helper/ns/$ns"
	;;

	status)
		get_namespaces "${pargs[1]:-}"

		for n in $namespaces; do
			echo_title $n

			features=(${pargs[@]:2})

			if [[ ${#features[@]} == 0 ]]; then
				systemctl list-dependencies netns-helper@$n.target
			else
				for feature in ${features[@]}; do
					systemctl status netns-helper-$feature@$n.service
				done
			fi
		done
	;;

	start)
		get_namespaces "${pargs[1]:-}"

		for n in $namespaces; do
			echo "Starting namespace \`$n\`..."
			systemctl start netns-helper@$n.target

			features=(${pargs[@]:2})

			if [[ ${#features[@]} > 0 ]]; then
				for feature in ${features[@]}; do
					echo "Starting feature \`$feature\` in \`$n\`..."
					systemctl start netns-helper-$feature@$n.service "${flags[@]}"
				done
			fi
		done
	;;

	stop)
		get_namespaces "${pargs[1]:-}"

		for n in $namespaces; do
			features=(${pargs[@]:2})

			if [[ ${#features[@]} == 0 ]]; then
				echo "Stopping namespace \`$n\`..."
				systemctl stop netns-helper@$n.target
			else
				for feature in ${features[@]}; do
					echo "Stopping feature \`$feature\` in \`$n\`..."
					systemctl stop netns-helper-$feature@$n.service "${flags[@]}"
				done
			fi
		done
	;;

	restart)
		get_namespaces "${pargs[1]:-}"

		for n in $namespaces; do
			features=(${pargs[@]:2})

			if [[ ${#features[@]} == 0 ]]; then
				echo "Restarting namespace \`$n\`..."
				systemctl restart netns-helper@$n.target
			else
				for feature in ${features[@]}; do
					echo "Restarting feature \`$feature\` in \`$n\`..."
					systemctl restart netns-helper-$feature@$n.service "${flags[@]}"
				done
			fi
		done
	;;

	addr)
		get_namespaces ${pargs[1]:-}

		for n in $namespaces; do
			echo_title $n
			ip netns exec $n ip addr
		done
	;;

	route)
		get_namespaces ${pargs[1]:-}

		for n in $namespaces; do
			echo_title $n

			echo_subtitle "IPv4"
			ip netns exec $n ip route

			echo_subtitle "IPv6"
			ip netns exec $n ip -6 route
		done
	;;

	ruleset)
		get_namespaces ${pargs[1]:-}

		for n in $namespaces; do
			echo_title $n
			ip netns exec $n nft list ruleset
		done
	;;

	exec)
		get_namespaces ${pargs[1]:-}

		for n in $namespaces; do
			echo_title $n
			ip netns exec $n "$@"
		done
	;;

	list)
		systemctl list-units --all --type target 'netns-helper*'
	;;

	*)
		>&2 echo 'Invalid command.'
		exit 1
	;;
esac

_netns_helper_systemctl()
{
	local -r mode=$1; shift 1
	systemctl $mode --full --legend=no --no-pager --plain "$@" 2>/dev/null
}

_netns_helper_find_namespaces()
{
	_netns_helper_systemctl --system list-units --type target "netns-helper@*.target" --all | sed -r 's/^(\s*)netns-helper@(\S+)\.target(.*)/\2/'
}

_netns_helper_find_services()
{
	_netns_helper_systemctl --system list-unit-files "$1*"
	_netns_helper_systemctl --system list-units --all "$1*"
}

_netns_helper_find_namespace_services()
{
	local -r namespaces=$(_netns_helper_find_namespaces)

	for ns in $namespaces; do
		_netns_helper_systemctl --system list-dependencies "netns-helper@${ns}.target" \
			 | sed -r 's/^(\s*)(.*)/\2/' \
			 | grep -v -G '^netns-helper.*'
	done
}

_netns_helper_completion()
{
	local -A switches=(
		[--now]=1 [--overwrite]=1 [--parent_if]=2 [--mac]=2 [--help]=1 [--macvlan_mode]=2
		[--privacy_ext]=2 [--ipaddr4]=2 [--ipaddr4_peer]=2 [--ipaddr6]=2 [--ipaddr6_peer]=2
		[--default_gateway4]=2 [--default_gateway6]=2 [--gateway_metric]=2 [--bridge_interface]=2)
	local -A actions=(
		[enable]=1 [disable]=1 [add-service]=1 [remove-service]=1 [status]=1 [start]=1
		[restart]=1 [stop]=1 [addr]=1 [purge]=1 [route]=1 [ruleset]=1 [exec]=1)
	local -A features=([dhcp]=1 [dhcp6]=1 [macvlan]=1 [nat]=1 [bridged]=1 [dnsmasq]=1 [dhcpcd]=1)
	local command command_index parguments=()

	local cur_word="${COMP_WORDS[COMP_CWORD]}"

	local prev_word=
	for i in "${!COMP_WORDS[@]}"; do
		[[ $i == $COMP_CWORD || $i == 0 ]] && continue

		local word="${COMP_WORDS[i]}"

		if [[ "$word" =~ ^-.* ]]; then
			unset switches["$word"]
		else
			if [[ ! "$prev_word" =~ ^-.* || "${switches[$prev_word]:-}" == 1 && ${actions[$word]+_} ]]; then
				command_index=${#parguments[@]}
				command="$word"
				actions=()
			fi
			parguments+=("$word")
		fi

		prev_word="$word"
	done

	if [[ "$cur_word" =~ ^-.* ]]; then
		COMPREPLY=($(compgen -W "$(echo ${!switches[@]})" -- "$cur_word"))
	else
		case "$command" in
			enable | disable)

				if (( command_index+1 >= ${#parguments[@]} )); then
					COMPREPLY=($(compgen -W "$(_netns_helper_find_namespaces)" -- "$cur_word"))
				else
					for ((i=command_index+2; i<${#parguments[@]}; i++)); do
						unset features["${parguments[$i]}"]
					done

					COMPREPLY=($(compgen -W "$(echo ${!features[@]})" -- "$cur_word"))
				fi
			;;

			start | restart | stop | status | addr | purge | route | ruleset | exec)
				if (( command_index+1 >= ${#parguments[@]} )); then
					COMPREPLY=($(compgen -W "$(_netns_helper_find_namespaces)" -- "$cur_word"))
				fi
			;;

			add-service)
				if (( command_index+1 >= ${#parguments[@]} )); then
					comps=$(_netns_helper_find_services "$cur_word")
					COMPREPLY=($(compgen -o filenames -W "$comps" -- "$cur_word"))
					compopt -o filenames
				elif (( command_index+2 >= ${#parguments[@]} )); then
					COMPREPLY=($(compgen -W "$(_netns_helper_find_namespaces)" -- "$cur_word"))
				fi
			;;

			remove-service)
				if (( command_index+1 >= ${#parguments[@]} )); then
					COMPREPLY=($(compgen -o filenames -W "$(_netns_helper_find_namespace_services)" -- "$cur_word"))
					compopt -o filenames
				fi
			;;

			*)
				COMPREPLY=($(compgen -W "$(echo ${!actions[@]})" -- "$cur_word"))
			;;
		esac
	fi
}

complete -F _netns_helper_completion netns-helper
